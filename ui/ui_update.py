

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(300, 130)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(Dialog.sizePolicy().hasHeightForWidth())
        Dialog.setSizePolicy(sizePolicy)
        Dialog.setMinimumSize(QtCore.QSize(300, 130))
        Dialog.setMaximumSize(QtCore.QSize(300, 130))
        self.gridLayoutWidget = QtWidgets.QWidget(Dialog)
        self.gridLayoutWidget.setGeometry(QtCore.QRect(10, 10, 281, 111))
        self.gridLayoutWidget.setObjectName("gridLayoutWidget")
        self.gridLayout = QtWidgets.QGridLayout(self.gridLayoutWidget)
        self.gridLayout.setContentsMargins(5, 5, 5, 5)
        self.gridLayout.setObjectName("gridLayout")
        self.progress_bar = QtWidgets.QProgressBar(self.gridLayoutWidget)
        self.progress_bar.setProperty("value", 0)
        self.progress_bar.setAlignment(QtCore.Qt.AlignCenter)
        self.progress_bar.setOrientation(QtCore.Qt.Horizontal)
        self.progress_bar.setObjectName("progress_bar")
        self.gridLayout.addWidget(self.progress_bar, 1, 0, 1, 1)
        self.install_label = QtWidgets.QLabel(self.gridLayoutWidget)
        self.install_label.setEnabled(False)
        self.install_label.setObjectName("install_label")
        self.gridLayout.addWidget(self.install_label, 2, 0, 1, 1)
        self.restart_label = QtWidgets.QLabel(self.gridLayoutWidget)
        self.restart_label.setEnabled(False)
        self.restart_label.setObjectName("restart_label")
        self.gridLayout.addWidget(self.restart_label, 3, 0, 1, 1)
        self.label = QtWidgets.QLabel(self.gridLayoutWidget)
        self.label.setObjectName("label")
        self.gridLayout.addWidget(self.label, 0, 0, 1, 1)

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Dialog"))
        self.install_label.setText(_translate("Dialog", "2. Installation"))
        self.restart_label.setText(_translate("Dialog", "3. Redémarrage"))
        self.label.setText(_translate("Dialog", "1. Téléchargement de la dernière version"))

