# coding=utf-8
__author__ = 'etcher3rd'

from distutils.core import setup
from main import __version__
from shutil import rmtree
from os.path import exists
import requests
import py2exe

# C:\Python34\lib\site-packages\requests


setup(
    requires=['requests', 'PyQt5'],
    name="EKPI v{}".format(__version__),
    data_files=[
        ('', [r'C:\Python34\lib\site-packages\requests\cacert.pem']),
        # ('imageformats', ['C:\\Python34/Lib/site-packages/PyQt4/plugins/imageformats/qjpeg4.dll',
        #                   'C:\\Python34/Lib/site-packages/PyQt4/plugins/imageformats/qgif4.dll',
        #                   'C:\\Python34/Lib/site-packages/PyQt4/plugins/imageformats/qico4.dll',
        #                   'C:\\Python34/Lib/site-packages/PyQt4/plugins/imageformats/qmng4.dll',
        #                   'C:\\Python34/Lib/site-packages/PyQt4/plugins/imageformats/qsvg4.dll',
        #                   'C:\\Python34/Lib/site-packages/PyQt4/plugins/imageformats/qtiff4.dll'
        # ])
    ],
    windows=[
        {
            'script': 'main.py',
            "icon_resources": [(1, "./ui/ekpi.ico")],
            'dest_base': 'ekpi',
        }
    ],
    options={
        'py2exe': {
            'includes': [
                'sip',
                'requests',
            ],
            # 'excludes': [
            # 'email',
            #     'distutils',
            #     'collections',
            #     'encoding',
            # ],
            # 'compressed': True,
            # 'bundle_files': 1,
        }
    },
    # zipfile = None,
)
