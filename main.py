# coding=utf-8
__author__ = 'etcher3rd'
__version__ = "alpha31"
__guid__ = '5b6dab14-577b-48b8-955a-80c0112a2aee'

import sys
import ctypes
from os import _exit

if __name__ == "__main__":
    if hasattr(sys, 'frozen'):
        sys.stdout = open("stdout.log", "w")
        sys.stderr = open("stderr.log", "w")
        # noinspection PyBroadException
        try:
            ctypes.windll.shell32.SetCurrentProcessExplicitAppUserModelID(__guid__)
        except:
            pass
    # noinspection PyUnresolvedReferences
    import ekpi
    _exit(0)

