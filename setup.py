# coding=utf-8
__author__ = 'etcher3rd'

# noinspection PyPackageRequirements
from cx_Freeze import Executable, setup
import sys
from main import __version__

build_exe_options = {
    'packages': ['os', 'socket', 'select', 'sys', 'threading', 'time', 'json', 'datetime'],
    'excludes': ['tkinter'],
    'include_files': [(r"C:\Python34\Lib\site-packages\PyQt5\plugins\sqldrivers", "sqldrivers"),
                      (r'C:\Python34\lib\site-packages\requests\cacert.pem', 'cacert.pem')
    ]
}

base = None

if sys.platform == 'win32':
    base = 'Win32GUI'
    # base = 'console'

setup(name='EKPI',
      version=__version__.replace('alpha', '0.0.0.').replace('beta', '0.0.'),
      description="Etcher's Katze's Pit Installer",
      options={'build_exe': build_exe_options},
      executables=[
          Executable('main.py',
                     targetName='ekpi.exe',
                     base=base,
                     icon='./ui/ekpi.ico',
                     # targetDir='build/EAMIv{}'.format(__version__)
          )],
      requires=['requests', 'PyQt5'])
