# EKPI - Etcher's Katze's Pit Installer
## Deux petits mots vite fait

Ce petit programme en Python sert à installer et à tenir à jour les différents pits de Katze (Ka, Mi, Huey et FC3), les scripts nécessaires à leur fonctionnement (script SIOC, exports DCS), et le KatzeLink.

Le KatzeLink est un programme, en Python également, écrit par Katze, et qui sert d'interface entre SIOC et l'interface Web des diffrents pits. Par souci de facilité (pour vous, hein, pas pour moi =) ), le KatzeLink est proposé en version compilée par mes soins, pour ne pas que vous ayez à installer le framework Python par dessus. A l'occasion, je me pencherai sur son cas (interface, connexions multiples, ...).

Tout ce petit monde est hébergé sur Github, de façon à nous permettre, à Katze et moi-même, de travailler de concert. EKPI se sert également de Github pour vérifier s'il existe des nouvelles versions des différents éléments, et les télécharger.

Voici nos adresses respectives:

 * [KaTZe](https://github.com/3rd-KaTZe)
    
 * [etcher](https://github.com/etcher3rd)
 
Via ces deux adresses, vous aurez accès à tout le code et à toutes les ressources que nous utilisons.

## Utilisation

EKPI est contenu dans un fichier ZIP standalone, vous pouvez le décompresser n'importe où sur le disque.

Les options sont sauvegardées dans le fichier `ekpi.config`, qui est au format JSON et éditable comme du texte.

Le programme écrit sont journal dans le fichier `ekpi.log`, et le bootstrap autour de l'exécutable écrit son journal dans les fichiers
`stderr.log` et `stdout.log`.

## Versions `alpha`, `beta` et mises à jours

Le programme est actuellement en version alpha, ce qui veut dire que je ne garantis pas sa stabilité ou la sécurité de vos fichiers.

Pour les premières utilisations, je vous recommance **fortement** d'utiliser le switch "`test`", qui vous permettra de faire tourner le programme sans mettre vos fichiers en danger; EKPI fonctionnera normalement, mais au lieu de se vautrer dans vos répertoires `Saved Games`et `SIOC`, il installera tout à côté de `ekpi.exe`, ce qui vous permettra de tester son bon fonctionnement.

###### Le mode test s'active comme suit:
Créez un raccourci avec la ligne de commande suivante:
```
...\ekpi.exe test
```
Le programme confirmera qu'il est en mode test au moyen d'une couleur jaune canari particulièrement vomitive.

### Mises à jour

Si tout se passe bien EKPI devrait se mettre à jour tout seul, sans intervention de votre part. En cas de pépin, il faudra télécahrger la nouvelle version à la min et éventuellement migrer le fichier `ekpi.config` si vous voulez conserver vos paramètres et l'historique des fichiers déjà installés.

## Résolution des problèmes

En cas de problèmes, si vous avez l'esprit aventureux, vous pouvez toujours:

* supprimer ou renommer le fichier `ekpi.config`
* lire le fichier `ekpi.log` et tenter de voir par vous-même ce qui ne va pas

### Rapport de bug

En cas de plantage répétés et insoluble, n'hésitez pas à faire remonter le problème. J'aurai besoin des 3 fichiers `*.log`, et éventuellement une description du problème, ainsi que de la marche à suivre pour le provoquer.

Dans la mesure du possible, je vous demande d'utiliser [la page "Issues" du projet EKPI](https://github.com/etcher3rd/EKPI/issues) pour signaler les bugs, ou suggérer des améliorations. Celait me simplifierait beaucoup la vie.
