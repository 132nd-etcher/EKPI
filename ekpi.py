# coding=utf-8

__author__ = 'etcher3rd'
# noinspection PyUnresolvedReferences
from main import __version__, __guid__

import sys
import builtins
# noinspection PyProtectedMember
from os import environ, _exit, mkdir, listdir, walk, startfile
from requests import head as requests_head
from requests import get as requests_get
from shutil import copytree, copy, move, rmtree
from os.path import join, dirname, abspath, exists, split
from custom_logging import mkLogger, logged
from logging import Handler, Formatter
from json import dumps, loads
from PyQt5 import QtCore, QtGui
from PyQt5.QtWidgets import QDialog, QMainWindow, QApplication, QMessageBox, QFileDialog, QPushButton
from queue import Queue
from urllib import request
from random import choice, shuffle
from string import ascii_uppercase, digits
from tempfile import gettempdir
from hashlib import sha1
from subprocess import call
from abc import abstractmethod, abstractproperty, ABCMeta
from zipfile import ZipFile, ZIP_LZMA, BadZipfile
from webbrowser import open as web_open
from winreg import ConnectRegistry, OpenKey, QueryValueEx, HKEY_CURRENT_USER
from ui import ui_update as qt_update_ui, ui_main as qt_main_ui
from subprocess import Popen
from csv import reader as csv_reader, writer as csv_writer
from pywin32_testutil import check_is_admin
from win32process import CreateProcess, CREATE_NEW_CONSOLE, STARTUPINFO

# Failing SSL is considered bad practice, so better get our ass covered in case of MITM attack while contacting Github
# (can you feel the irony ? Jeez ...)
cert = './cacert.pem'
environ['REQUESTS_CA_BUNDLE'] = cert

# region Regex
RE_QT_IP_ADDRESS_VALIDATOR = QtCore.QRegExp("^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\."
                                            "(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\."
                                            "(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\."
                                            "(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$")
# endregion

# Gimme cats ! Because a cat is fine too (just kidding, Katze =p)
random_katze = [
    'http://funmozar.com/wp-content/uploads/2014/07/funny-cat-05.jpg',
    'http://www.petpicturegallery.com/pictures/cats/funnycat/134-cat_funnycat_funny_cat_12.jpg',
    'http://www.2lucu.com/wp-content/uploads/2014/10/funny-cat-jump.jpg',
    'http://www.stylespalace.com/wp-content/uploads/2014/12/Funny_Cat_Face515.jpg',
    'http://7-themes.com/data_images/out/62/6985370-funny-cat.jpg',
    'http://www.easyblindsonline.co.uk/blog/wp-content/uploads/2014/11/Funny-Cat-Face-1024x640.jpg',
    'http://www.funnycatpictures.net/wp-content/uploads/2012/07/funny-cat-real-pikachu.jpg',
    'https://thechive.files.wordpress.com/2010/12/funny-cat-faces-1.jpg',
    'https://3.bp.blogspot.com/-cUGb8AiQ5NA/TWVMY1RjkgI/AAAAAAAAAWo/C3u_ex3N5mM/s1600/funny-sad-cat.jpg',
    'http://www.bestweekofyoursummer.com/wp-content/uploads/2013/03/sad-emoticon-funny-cat-face.jpg',
    'http://quicklol.com/wp-content/uploads/2013/01/cat-with-a-funny-face.jpg',
    'http://smilepanic.com/wp-content/uploads/funny-face-cat.jpg',
    'http://images4.fanpop.com/image/photos/23500000/Epic-Face-Kitteh-lol-23579309-400-269.jpg',
    'http://www.picsjoy.com/wp-content/uploads/2013/05/funny-cat-face-wallpaper.jpg',
    'https://i.imgur.com/WpI9d.jpg',
    'http://www.bambog.com/wp-content/uploads/2014/08/cat-face.jpg',
    'http://www.humwallpaper.com/wp-content/uploads/2014/08/funny-cat-faces-111.jpg',
    'http://www.oddanimals.com/images/oddanimals063.jpg',
    'http://allhumorpic.com/wp-content/uploads/funny-cat-face-whiskers.jpg',
    'http://1funny.com/wp-content/uploads/2012/08/cat-face.jpg',
    'http://www.amusingtime.com/images/017/funny-cat-face-picture.jpg',
    'http://stuffpoint.com/cats/image/87092-cats-funny-cat-face.jpg',
    'http://forum.lowyat.net/uploads//avatars/av-624089-1360816266.jpg',
    'http://cdn77.sadanduseless.com/wp-content/uploads/2014/06/funny-cat4.jpg',
    'https://s-media-cache-ak0.pinimg.com/236x/0b/ba/0c/0bba0ca573083db5647ce05106c21aa0.jpg',
    'http://www.funnycatpix.com/_pics/closeup_catface.jpg',
    'https://thechive.files.wordpress.com/2010/12/funny-cat-faces-9.jpg',
    'http://cdn.themetapicture.com/media/funny-cute-cat-leaning-sad-face.jpg',
    'http://www.funlyest.com/files/articles/685/1395824346.jpg',
    'http://data1.whicdn.com/images/38697762/large.jpg',
    'http://efunnyimages.com/wp-content/uploads/funny-sad-cute-cat-face-crying.jpg',
    'http://www.funnycatpictures.net/wp-content/uploads/2012/07/funny-cat-face.jpg',
    'http://www.technocrazed.com/wp-content/uploads/2013/07/Cats-making-funny-faces-2.jpg',
    'http://www.funnycatpix.com/_pics/Cat_Face342.jpg',
    'http://www.funnycatsite.com/pictures/Funny_Cat_Face623.jpg',
    'http://cutecatshq.com/wp-content/uploads/2013/04/Sphynx-cat-face.jpg',
    'http://funmozar.com/wp-content/uploads/2014/07/funny-cat-07.jpg',
    'https://i.ytimg.com/vi/p2H5YVfZVFw/hqdefault.jpg',
    'http://www.2lucu.com/wp-content/uploads/2014/10/funny-cat-gallery.jpg',
    'http://www.petsworld.in/blog/wp-content/uploads/2015/01/fun.jpg',
    'http://images2.fanpop.com/image/photos/13600000/funny-cat-cats-13624271-800-600.jpg',
    'http://images1.fanpop.com/images/image_uploads/Funny-Cat-Pictures-cats-935742_555_555.jpg',
    'http://www.funniestfive.com/wp-content/uploads/2014/07/Funny-Cat-5.jpg',
    'http://www.funnypica.com/wp-content/uploads/2012/11/Funny-Cats-Big-Eyed-Cat.jpg',
    'http://www.fantom-xp.com/wallpapers/33/Funny_Cat_desktop_backgrounds.jpg',
    'http://dougleschan.com/the-recruitment-guru/wp-content/uploads/2014/07/funny-cat.jpg',
    'https://1.bp.blogspot.com/-vnOpCrf-Srg/T1MLhIs6QZI/AAAAAAAAC_U/dKE8IqybnoI/s1600/funny+cat+photos+(70).jpg',
    'https://3.bp.blogspot.com/-cD3Q0aIZ_qo/TzQZzcUf6WI/AAAAAAAAoHM/opAP1e4D2Yk/s1600/Funny+Cat+Pics+%2528172%2529.jpg',
    'http://static.themetapicture.com/media/funny-cat-kitten-derp-face.jpg',
    'https://thechive.files.wordpress.com/2010/12/funny-cat-faces-14.jpg',
]

# Put this bunch of furballs in the washing machine and let it spin a bit
shuffled_katze = [x for x in random_katze]
shuffle(shuffled_katze)

index = {
    'log_levels': {
        0: "1: Gilles",
        1: "2: Infos",
        2: "3: Avertissements",
        3: "4: Erreurs",
        4: "5: Erreurs critiques",
        5: "6: Echecs",
    },
    'gh_repos': {
        0: 'KaTZe',
        1: 'etcher',
    },
    'fc3_pit': {
        0: 'KaTZ-Pit_F15.html',
        1: 'KaTZ-Pit_Mig29.html',
        2: 'KaTZ-Pit_SU25.html',
        3: 'KaTZ-Pit_SU27.html',
        4: 'KaTZ-Pit_SU33.html',
    },
}

gh = {
    'atom': {
        'huey_pit': {'name': 'KaTZ-Pit_UH1', 'latest': None},
        'mi8_pit': {'name': 'KaTZ-Pit_Mi8', 'latest': None},
        'ka50_pit': {'name': 'KaTZ-Pit_Ka50', 'latest': None},
        'fc3_pit': {'name': 'KaTZ-Pit_FC3', 'latest': None},
        'sioc': {'name': 'SIOC', 'latest': None},
        'link': {'name': 'Link', 'latest': None},
        'data_dico': {'name': 'Link', 'latest': None},
        'export': {'name': 'Export_DCS', 'latest': None},
    },
    'branch': {
        0: 'master',
        1: 'develop',
    },
    'owner': {
        0: '3rd-KaTZe',
        1: 'etcher3rd',
    },
    'token': environ['GITHUB_TOKEN'] if 'GITHUB_TOKEN' in environ.keys() else None,
}


# noinspection PyBroadException
class BuiltinWrapper():
    old_requests_get = requests_get
    old_requests_head = requests_head
    old_mkdir = mkdir
    old_rmtree = rmtree
    old_copytree = copytree
    old_move = move
    old_copy = copy
    oldopen = builtins.open

    @logged
    def __init__(self):
        pass

    # noinspection PyBroadException
    @staticmethod
    def new_open(*args, **kwargs):
        if not args[0][-8] == 'ekpi.log':
            logger.debug("open - path: {}".format(args[0]))
        try:
            return BuiltinWrapper.oldopen(*args, **kwargs)
        except:
            logger.exception("open - échec lors de l'ouverture du fichier")

    @staticmethod
    def _requests_wrapper(func, *args, **kwargs):
        logger.debug("requests_wrapper - {} - requête HTTP: {}".format(func.__name__, args[0]))
        headers = {'Authorization': 'token {}'.format(gh['token'])}
        if 'headers' in kwargs.keys():
            headers = dict(list(headers.items()) + list(kwargs['headers'].items()))
            kwargs.pop('headers')
        try:
            if 'api.github.com' in args[0]:
                if gh['token'] is not None:
                    r = func(*args, headers=headers, **kwargs)
                elif config.gh_user and config.gh_pass:
                    r = func(*args, auth=(config.gh_user, config.gh_pass), **kwargs)
                else:
                    r = func(*args, **kwargs)
                if r and r.headers and 'X-RateLimit-Remaining' in r.headers.keys():
                    main_ui.sig_req_update.emit(r.headers['X-RateLimit-Remaining'])
            else:
                r = func(*args, **kwargs)
            return r
        except:
            logger.exception("requests_wrapper - {} - erreur lors de la requête".format(func.__name__, args[0]))
            return

    # noinspection PyBroadException
    @staticmethod
    def new_requests_get(*args, **kwargs):
        return BuiltinWrapper._requests_wrapper(BuiltinWrapper.old_requests_get, *args, **kwargs)

    # noinspection PyBroadException
    @staticmethod
    def new_requests_head(*args, **kwargs):
        return BuiltinWrapper._requests_wrapper(BuiltinWrapper.old_requests_head, *args, **kwargs)

    # noinspection PyBroadException
    @staticmethod
    def new_mkdir(*args, **kwargs):
        logger.debug("mkdir - création du répertoire: {}".format(args[0]))
        try:
            BuiltinWrapper.old_mkdir(*args, **kwargs)
        except:
            logger.exception("mkdir - impossible de créer le chemin suivant: {}".format(args[0]))
            return
        return True

    # # noinspection PyBroadException
    # @staticmethod
    # def new_rename(*args, **kwargs):
    # try:
    # logger.debug("rename - renommage du fichier: {} -> {}".format(args[0], args[1]))
    # BuiltinWrapper.old_rename(*args, **kwargs)
    # except:
    # logger.exception("rename - erreur lors du renommage".format(args[0]))
    # return
    #     return True

    # noinspection PyBroadException
    @staticmethod
    def new_rmtree(*args, **kwargs):
        try:
            logger.debug("rmtree - suppression du répertoire {}".format(args[0]))
            BuiltinWrapper.old_rmtree(*args, **kwargs)
        except:
            logger.exception("rmtree - erreur pendant la suppression")
            return
        return True

    # noinspection PyBroadException
    @staticmethod
    def new_copytree(*args, **kwargs):
        try:
            logger.debug("copytree - copie du répertoire: {} -> {}".format(args[0], args[1]))
            BuiltinWrapper.old_copytree(*args, **kwargs)
        except:
            logger.exception("copytree - erreur pendant la copie")
            return
        return True

    # noinspection PyBroadException
    @staticmethod
    def new_move(*args, **kwargs):
        try:
            logger.debug("move - déplacement: {} -> {}".format(args[0], args[1]))
            BuiltinWrapper.old_move(*args, **kwargs)
        except:
            logger.exception("move - erreur pendant le déplacement")
            return
        return True

    # noinspection PyBroadException
    @staticmethod
    def new_copy(*args, **kwargs):
        try:
            logger.debug("copy - copie du fichier: {} -> {}".format(args[0], args[1]))
            BuiltinWrapper.old_copy(*args, **kwargs)
        except:
            logger.exception("copy - erreur pendant la copie")
            return
        return True

    # redefines builtins to force low-level logging
    @staticmethod
    def redeclare_builtins():
        global copy, copytree, rmtree, requests_head, requests_get, mkdir  # , rename
        copy = BuiltinWrapper.new_copy
        copytree = BuiltinWrapper.new_copytree
        rmtree = BuiltinWrapper.new_rmtree
        # rename = BuiltinWrapper.new_rename
        requests_get = BuiltinWrapper.new_requests_get
        requests_head = BuiltinWrapper.new_requests_head
        mkdir = BuiltinWrapper.new_mkdir
        # builtins.open = BuiltinWrapper.new_open


# endregion


class Config():
    """
    Set it all up

    This holds everything that is accessible to the user, plus some more. It also writes/reads to the config file,
    so nothing's lost between runs.
    """
    defaults = {
        'link_hote': '127.0.0.1',
        'link_port': '9000',
        'ts_hote': '127.0.0.1',
        'ts_port': '10500',
        'pit_hote': 'WIP',
        'pit_port': 'WIP',
        'sioc_hote': '127.0.0.1',
        'sioc_port': '8092',
        'sioc_plage': '0',
        'exports_timing_fast': '100',
        'exports_timing_slow': '500',
        'exports_timing_fps': '5',
        'log_level': 1,
        'branch': 0,
        'repo': 0,
        'fc3_pit': 0,
    }

    def __init__(self):
        self.__dict__['config'] = {}
        self.__dict__['path'] = None
        if hasattr(sys, 'frozen'):
            self.path = join(dirname(abspath(sys.executable)), 'ekpi.config')
        else:
            self.path = join(dirname(abspath(__file__)), 'ekpi.config')
        self.read()

    def read(self):
        if exists(self.path):
            with open(self.path) as f:
                self.__dict__['config'] = loads(f.read())

    def write(self):
        with open(self.path, mode='w') as f:
            f.write(dumps(self.__dict__['config'], indent=True, sort_keys=True))

    def __getattr__(self, key):
        for d in [self.__dict__, self.__dict__['config'], self.defaults]:
            try:
                return d[key]
            except KeyError:
                pass
        return ''

    def __setattr__(self, key, value):
        if key in self.__dict__.keys():
            self.__dict__[key] = value
        else:
            self.__dict__['config'][key] = value
            self.write()

    def __getitem__(self, key):
        return getattr(self, key)

    def __setitem__(self, key, value):
        self.__setattr__(key, value)


class ManagedStuff(metaclass=ABCMeta):
    """
    Real inspiring name, isn't it ? It's basically "stuff" that is somehow "managed". Mind blown.

    This is basically an abstract class to enforce consistency between all the atoms of the Ktaze's pit.
    """

    logger = None

    @logged
    def __init__(self):
        pass

    @abstractproperty
    def dic_name(self):
        pass

    @abstractproperty
    def install_dir(self):
        pass

    @abstractmethod
    def run(self):
        pass

    @abstractmethod
    def install_latest(self):
        pass

    @abstractproperty
    def can_run(self):
        pass

    def update_remote_sha(self):
        self.logger.debug('{}: {}'.format(self.dic_name, self.latest_version))
        config['{}_remote_sha'.format(self.dic_name)] = self.latest_version

    def update_local_sha(self):
        # The local sha is an aggregated SHA for all relevant files in the installation directory of the atom
        # It's used to compare changes between runs of EKPI
        self.logger.debug('{}: {}'.format(self.dic_name, self.local_sha))
        config["{}_local_sha".format(self.dic_name)] = self.local_sha

    @property
    def repo_name(self):
        # noinspection PyTypeChecker
        return gh['atom'][self.dic_name]['name']

    @property
    def latest_version(self):
        # noinspection PyTypeChecker
        return gh['atom'][self.dic_name]['latest']

    @property
    def remote_sha(self):
        return config['{}_remote_sha'.format(self.dic_name)]

    @property
    def is_installed(self):
        ret = not self.remote_sha == ''
        self.logger.debug('{}: {}'.format(self.dic_name, ret))
        return ret

    @property
    def has_update(self):
        self.logger.debug(self.dic_name)
        if self.latest_version is None:
            self.logger.debug('{}: aucune dernière version disponible'.format(self.dic_name))
            return
        self.logger.debug('{}: {}'.format(self.dic_name, not self.remote_sha == self.latest_version))
        return not self.remote_sha == self.latest_version

    @property
    def download_url(self):
        return 'https://github.com/{}/{}/archive/{}.zip' \
            .format(gh['owner'][config.repo], self.repo_name, gh['branch'][config.branch])

    @property
    def local_sha(self):
        return path_checksum(self.install_dir)

    @property
    def is_corrupt(self):
        return not self.local_sha == config["{}_local_sha".format(self.dic_name)]


# noinspection PyBroadException
class KatzePit(ManagedStuff, metaclass=ABCMeta):
    def __init__(self, dic_name):
        ManagedStuff.__init__(self)
        self.__dic_name = dic_name

    @property
    def dic_name(self):
        return self.__dic_name

    @property
    def install_dir(self):
        return abspath('./pits/{}'.format(self.dic_name))

    @property
    def html_file(self):
        if exists(self.install_dir):
            for x in listdir(self.install_dir):
                if x[-5:] == ".html":
                    if self.dic_name == 'fc3_pit' and not x == index['fc3_pit'][config.fc3_pit]:
                        continue
                    return abspath(join(self.install_dir, x))
            self.logger.warning('{}: le fichier HTML n\'a pas été trouvé;'
                                ' impossible de lancer le Pit'.format(self.dic_name))
        return None

    @property
    def can_run(self):
        return self.html_file is not None

    @property
    def js_file(self):
        return abspath(join(self.install_dir, 'js/serverws.js'))

    def install_latest(self):
        self.logger.debug("{}: installation de la dernière version".format(self.dic_name))
        if not self.has_update and not self.is_corrupt:
            self.logger.warning("{}: la dernière version est déjà installée".format(self.dic_name))
            return
        tmpfile = tmp_path()
        # noinspection PyTypeChecker
        if not Defer.download(self.download_url, tmpfile, download_nice_name=self.dic_name, redirect=True):
            self.logger.error("{}: erreur lors du téléchargement".format(self.dic_name))
            return
        tmpdir = tmp_path()
        if not Defer.unzip(tmpfile, tmpdir):
            return
        if exists(self.install_dir):
            if not rmtree(self.install_dir):
                return
        if not exists('./pits'):
            mkdir('./pits')
        if not copytree(join(tmpdir, listdir(tmpdir)[0]), self.install_dir):
            return
        self.setup_link()  # also updates local SHA
        self.update_remote_sha()
        return True

    def setup_link(self):
        try:
            if exists(self.js_file):
                o = []
                with open(self.js_file) as f:
                    l = f.readlines()
                for x in l:
                    if 'ip : ' in x:
                        o.append('	ip : "{}",\n'.format(config.link_hote))
                    elif 'port : ' in x:
                        o.append('	port : {},\n'.format(config.link_port))
                    else:
                        o.append(x)
                with open(self.js_file, mode='w') as f:
                    f.writelines(o)
        except:
            self.logger.exception('')
            return
        self.update_local_sha()
        return True

    def run(self):
        self.logger.debug("{}: démarrage du pit".format(self.dic_name))
        if self.html_file:
            web_open(self.html_file)


# noinspection PyBroadException
class SIOC(ManagedStuff):
    @logged
    def __init__(self):
        ManagedStuff.__init__(self)

    @property
    def dic_name(self):
        return 'sioc'

    @property
    def local_sha(self):
        return path_checksum(self.install_dir, includes=["katze.ssi"])

    @property
    def install_dir(self):
        return '.'

    @property
    def sioc_ini(self):
        if local_test:
            return './sioc.ini', None
        return [join(split(environ['APPDATA'])[0], 'Local\VirtualStore\Program Files (x86)\IOCards\SIOC\sioc.ini'),
                abspath(join(config.sioc_dir, 'sioc.ini'))]

    def __ini_file_getter(self, line):
        _len = len(line)
        try:
            for x in self.sioc_ini:
                if x is None or not exists(x):
                    continue
                with open(x) as f:
                    for l in f.readlines():
                        if l[:_len] == line:
                            return l[_len:-1]
            self.logger.error('valeur non trouvée: {}'.format(line))
        except:
            self.logger.error('erreur lors de la récupération de la valeur: {}'.format(line))

    def __ini_file_setter(self, line, value):
        if not check_is_admin() and 'Program Files' in config.sioc_dir:
            self.logger.warning('impossible de mettre à jour la valeur "{}" pour SIOC, EKPI n\'a pas été lancé en mode '
                                'administrateur'.format(line))
            return
        self.logger.debug('line: {}\tvalue: {}'.format(line, value))
        try:
            _len = len(line)
            value_found = False
            if not (exists(self.sioc_ini[0]) or exists(self.sioc_ini[1])):
                self.logger.error('aucun fichier "sioc.ini" trouvé')
                return
            for x in self.sioc_ini:
                if x is None or not exists(x):
                    continue
                with open(x, mode='r') as f:
                    r = f.readlines()
                for i in range(len(r)):
                    if r[i][:_len].lower() == line.lower():
                        r[i] = '{}{}\n'.format(line, value)
                        value_found = True
                if not value_found:
                    self.logger.error('valeur non trouvée: {}'.format(line))
                with open(x, mode='w') as f:
                    f.writelines(r)
        except:
            self.logger.exception("erreur lors de l'écriture de la valeur: {}".format(line))
        self.update_local_sha()

    @property
    def ini_config_file(self):
        return self.__ini_file_getter('Config_File=')

    @ini_config_file.setter
    def ini_config_file(self, value):
        self.__ini_file_setter('Config_File=', value)

    @property
    def ini_port(self):
        return self.__ini_file_getter('IOCP_port=')

    @ini_port.setter
    def ini_port(self, value):
        self.__ini_file_setter('IOCP_port=', value)

    def set_ini_port(self):
        self.ini_port = config.sioc_port

    def install_latest(self):
        self.logger.debug('{}: installation de la dernière version'.format(self.dic_name))
        tmpfile = tmp_path()
        if not Defer.download(self.download_url, tmpfile, download_nice_name=self.dic_name, redirect=True):
            self.logger.error('{}: erreur lors du téléchargement'.format(self.dic_name))
            return
        tmpdir = tmp_path()
        if not Defer.unzip(tmpfile, tmpdir):
            return
        if not copy(join(tmpdir, 'SIOC-{}/katze.ssi'.format(gh['branch'][config.branch])), join(self.install_dir)):
            self.logger.error('{}: erreur lors de la copie du fichier SSI'.format(self.dic_name))
            return
        self.logger.debug('{}: fichiers sioc.ini: {}'.format(self.dic_name, self.sioc_ini))
        self.ini_port = config.sioc_port
        self.ini_config_file = abspath('.\\katze.ssi')
        self.update_local_sha()
        self.update_remote_sha()
        return True

    def run(self):
        self.logger.debug('lancement de l\'éxécutable de SIOC')
        if exists(self.sioc_exe):
            CreateProcess(self.sioc_exe, None, None, None, 0,
                          CREATE_NEW_CONSOLE, None, self.install_dir, STARTUPINFO())
            # Popen(self.sioc_exe, cwd=dirname(self.sioc_exe))

    @property
    def sioc_exe(self):
        return abspath(join(config.sioc_dir, 'sioc.exe'))

    @property
    def can_run(self):
        return exists(self.sioc_exe)

    @property
    def is_corrupt(self):
        return not self.ini_config_file == abspath('.\\katze.ssi')


# noinspection PyBroadException
class KatzeLink(ManagedStuff):
    # noinspection PyPropertyDefinition

    logger = None
    _install_dir = abspath('./link')

    class DataDico(ManagedStuff):
        logger = None

        @logged
        def __init__(self):
            ManagedStuff.__init__(self)

        @property
        def install_dir(self):
            return KatzeLink._install_dir

        @property
        def dic_name(self):
            return 'data_dico'

        @property
        def download_url(self):
            return 'https://github.com/{}/{}/raw/{}/z_Data_Dico.csv'\
                .format(gh['owner'][config.repo], self.repo_name, gh['branch'][config.branch])

        @property
        def is_installed(self):
            return not self.remote_sha == ''

        @property
        def can_run(self):
            return False

        def run(self):
            return

        @property
        def local_sha(self):
            self.logger.debug('')
            return path_checksum(self.install_dir, includes=['z_Data_Dico.csv'])

        def install_latest(self):
            self.logger.debug('')
            tmp_file = tmp_path()
            if not Defer.download(self.download_url, tmp_file,
                                  download_nice_name='dictionnaire Link',
                                  redirect=True):
                self.logger.error('erreur lors du téléchargement')
                return
            if not copy(tmp_file, join(self.install_dir, 'z_Data_Dico.csv')):
                self.logger.error('erreur lors de la copie du dictionnaire Link')
                return
            self.update_local_sha()
            self.update_remote_sha()
            return True

    # noinspection PyPropertyDefinition,PyBroadException
    class LinkConfig():
        """
        CSV Wrapper for "config_Helo-Link.csv"
        """

        values = ['sioc_hote', 'sioc_port', 'sioc_plage', 'link_hote', 'link_port', 'ts_hote', 'ts_port']
        logger = None

        @logged
        def __init__(self, path_to_csv):
            self.d = {}
            self.p = path_to_csv
            self.__is_read = False

        def read(self):
            self.logger.debug('')
            if exists(self.p):
                with open(self.p, newline='') as csv_file:
                    for x in csv_reader(csv_file):
                        try:
                            self.d[x[0]] = x[1]
                        except IndexError:
                            continue
            self.__is_read = True

        def write(self):
            self.logger.debug('')
            if not self.__is_read:
                self.read()
            try:
                self.logger.debug('écriture du fichier de configuration du Link')
                with open(self.p, mode='w', newline='') as csv_file:
                    self.d['ts_port'] = config.ts_port
                    self.d['link_port'] = config.link_port
                    self.d['sioc_plage'] = config.sioc_plage
                    self.d['ts_hote'] = config.ts_hote
                    self.d['link_hote'] = config.link_hote
                    self.d['sioc_port'] = config.sioc_port
                    self.d['sioc_hote'] = config.sioc_hote
                    writer = csv_writer(csv_file)
                    for k in self.d.keys():
                        writer.writerow([k, self.d[k]])
            except:
                self.logger.exception('erreur lors de l\'écriture du fichier de configuration')

        def set_value(self, key, value):
            self.logger.debug('nouvelle valeur: {} = {}'.format(key, value))
            self.d.__setitem__(key, value)
            self.write()

        sioc_hote = property(lambda self: self.d.__getitem__('sioc_hote'),
                             lambda self, value: self.set_value('sioc_hote', value))
        sioc_port = property(lambda self: self.d.__getitem__('sioc_port'),
                             lambda self, value: self.set_value('sioc_port', value))
        sioc_plage = property(lambda self: self.d.__getitem__('sioc_plage'),
                              lambda self, value: self.set_value('sioc_plage', value))
        link_hote = property(lambda self: self.d.__getitem__('link_hote'),
                             lambda self, value: self.set_value('link_hote', value))
        link_port = property(lambda self: self.d.__getitem__('link_port'),
                             lambda self, value: self.set_value('link_port', value))
        ts_hote = property(lambda self: self.d.__getitem__('ts_hote'),
                           lambda self, value: self.set_value('ts_hote', value))
        ts_port = property(lambda self: self.d.__getitem__('ts_port'),
                           lambda self, value: self.set_value('ts_port', value))

    @logged
    def __init__(self):
        self.logger.debug('')
        ManagedStuff.__init__(self)
        self.config = KatzeLink.LinkConfig(self.config_file)

    @property
    def config_file(self):
        return abspath(join(self.install_dir, 'config_Helo-Link.csv'))

    @property
    def dic_name(self):
        return 'link'

    @property
    def install_dir(self):
        return self._install_dir

    @property
    def config_file_all_setup(self):
        self.logger.debug('')
        for x in self.config.values:
            if not self.config.d[x] == config[x]:
                return
        return True

    @property
    def local_sha(self):
        return path_checksum(self.install_dir, excludes=['config_Helo-Link.csv', 'KatzeLink.log', 'z_Data_Dico.csv'])

    def setup_config_file(self):
        self.logger.debug('')
        try:
            self.logger.debug('écriture des valeurs dans le fichier de configuration')
            for x in self.config.values:
                self.config.d[x] = config[x]
            self.config.write()
        except:
            self.logger.exception('erreur lors de l\'écriture du fichier CSV de configuration')
            return
        self.update_local_sha()
        return True

    def install_latest(self):
        self.logger.debug('')
        if self.latest_version is None:
            self.logger.warning("la dernière version n'est pas disponible")
            return
        if self.has_update or self.__is_corrupt:
            tmpfile = tmp_path()
            if not Defer.download(url=gh['atom']['link']['download_url'],
                                  target=tmpfile,
                                  size=gh['atom']['link']['download_size'],
                                  download_nice_name='Katze Link'):
                self.logger.error('erreur lors du téléchargement')
                return
            if exists(self.install_dir):
                if not rmtree(self.install_dir):
                    self.logger.error('erreur lors de la suppression de la version précédente')
                    return
            if not mkdir(self.install_dir):
                self.logger.error('erreur lors de la création du répertoire d\'installation')
                return
            if not Defer.unzip(tmpfile, self.install_dir):
                self.logger.error('erreur lors de la décompression des fichiers')
                return
        if data_dico.has_update or data_dico.is_corrupt:
            data_dico.install_latest()
        if not self.setup_config_file():
            self.logger.error('erreur lors de la configuration du fichier CSV')
            return
        self.update_remote_sha()
        return True

    @property
    def link_exe(self):
        for x in [abspath(join(self.install_dir, 'KatzeLink.exe')), abspath(join(self.install_dir, 'helo_link.exe'))]:
            return x

    def run(self):
        self.logger.debug('lancement de l\'éxécutable du KatzeLink')
        if exists(self.link_exe):
            CreateProcess(self.link_exe, None, None, None, 0,
                          CREATE_NEW_CONSOLE, None, self.install_dir, STARTUPINFO())

    @property
    def can_run(self):
        return self.is_installed and not self.is_corrupt

    @property
    def is_installed(self):
        return not self.remote_sha == '' and data_dico.is_installed

    @property
    def __is_corrupt(self):
        return not self.local_sha == config["{}_local_sha".format(self.dic_name)]

    @property
    def is_corrupt(self):
        return  self.__is_corrupt or data_dico.is_corrupt


# noinspection PyBroadException
class DCSExport(ManagedStuff):
    next_event_hook = [
        '\t-- EKPI -- NE PAS MODIFIER EN DESSOUS --\n',
        '\trendre_hommage_au_grand_Katze() -- ligne ajoutée par EKPI -- ne pas modifier\n',
        '\t-- EKPI -- NE PAS MODIFIER AU DESSUS --\n',
    ]
    logger = None

    @logged
    def __init__(self):
        self.logger.debug('')
        ManagedStuff.__init__(self)
        # self.insert_next_event_function = True
        # self.insert_next_event_hook_snippet = -1
        # self.insert_katze_dofile = True
        self.do_file_ok = False

    @property
    def dic_name(self):
        return 'export'

    @property
    def main_export_all_set(self):
        self.logger.debug('')
        self.insert_next_event_function = True
        self.insert_next_event_hook_snippet = -1
        self.logger.debug('')
        if not exists(self.install_dir):
            self.logger.debug('le répertoire d\'installation n\'existe pas')
            return
        if not exists(self.main_export_lua_file):
            self.logger.error('le fichier "export.lua" n\'existe pas')
            return
        else:
            self.logger.debug('ouverture du fichier "export.lua"')
            with open(self.main_export_lua_file) as f:
                r = f.readlines()
                if r[-1] != "dofile(lfs.writedir()..'/Scripts/katze.lua')\n":
                    return
                for x in r[:-1]:
                    if '/Scripts/katze.lua' in x:
                        return
                return True

    @property
    def local_sha(self):
        self.logger.debug('')
        return path_checksum(self.install_dir,
                             includes=['katze_config.lua', 'katze.lua'] + ['KTZ_SIOC_{}.lua'
                             .format(x) for x in ['FC3', 'KA50', 'Mi8', 'UH1']])

    @property
    def is_corrupt(self):
        if not all([exists(join(self.install_dir, 'KTZ_SIOC_{}.lua'.format(x)))
                    for x in ['FC3', 'KA50', 'Mi8', 'UH1']]
                + [exists(join(self.install_dir, x) for x in self.side_exports_names)]):
            return True
        return super(self).is_corrupt

    @property
    def install_dir(self):
        if local_test:
            return '.'
        return join(config.dcs_saved_games_dir, "scripts")

    @property
    def main_export_lua_file(self):
        return abspath(join(self.install_dir, "export.lua"))

    @property
    def side_exports_names(self):
        return ['KTZ_SIOC_{}.lua'.format(x) for x in ['FC3', 'KA50', 'Mi8', 'UH1']] \
               + ['katze.lua', 'common.lua', 'overload.lua', 'sioc.lua']

    @property
    def side_exports_local_paths(self):
        return [abspath(join(self.install_dir, x)) for x in self.side_exports_names]

    @property
    def side_exports_all_set(self):
        return all([exists(x) for x in self.side_exports_local_paths])

    @property
    def main_export_lua_file_backup(self):
        return abspath("{}.backup_ekpi".format(self.main_export_lua_file))

    @property
    def is_corrupt(self):
        return not all([self.main_export_all_set,
                        self.side_exports_all_set,
                        self.local_sha == config["{}_local_sha".format(self.dic_name)]])

    def setup_main_export(self):
        self.logger.debug('')
        if self.main_export_all_set:
            return
        try:
            if not exists(self.main_export_lua_file_backup):
                self.logger.debug('pas de backup trouvé; création')
                try:
                    copy(self.main_export_lua_file, self.main_export_lua_file_backup)
                except:
                    self.logger.exception('erreur lors de la création du backup')
                    return
            if exists(self.main_export_lua_file):
                self.logger.debug('le fichier "export.lua" existe déjà, lecture')
                o = []
                with open(self.main_export_lua_file) as f:
                    r = f.readlines()
                    for x in r:
                        if '/Scripts/katze.lua' in x:
                            continue
                        o.append(x)
                    o.append("dofile(lfs.writedir()..'/Scripts/katze.lua')\n")
                with open(self.main_export_lua_file, mode="w") as f:
                    f.writelines(o)
            else:
                self.logger.info('le fichier "export.lua" n\existe pas, création')
                with open(self.main_export_lua_file, mode="w") as f:
                    f.write("dofile(lfs.writedir()..'/Scripts/katze.lua')\n")
        except:
            self.logger.exception('une erreur innattendue s\'est produite')
            return
        self.logger.debug('export principal correctement installé')
        return True

    def install_sioc_config_lua(self):
        self.logger.debug('')
        try:
            self.logger.debug('ouverture de "katze_config.lua" en écriture')
            with open(join(self.install_dir, 'katze_config.lua'), mode='w') as f:
                f.write(
                    'siocConfig = {{hostIP = "{hote}", hostPort = {port}, plageSioc = {plage},'
                    ' timing_fast = {fast}, timing_slow = {slow}}}\n'

                    'k.config.sioc = {{hostIP = "{hote}", hostPort = {port}, plageSioc = {plage},'
                    ' timing_fast = {fast}, timing_slow = {slow}, fps={fps}}}\n'

                    'k.sioc.ip = "{hote}"\n'
                    'k.sioc.port = {port}\n'
                    'k.sioc.plage = {plage}\n'
                    'k.loop.sample.fast = {fast}\n'
                    'k.loop.sample.slow = {slow}\n'
                    'k.loop.sample.fps = {fps}\n'

                    .format(hote=config.sioc_hote, port=config.sioc_port, plage=config.sioc_plage,
                            fast=config.exports_timing_fast, slow=config.exports_timing_slow,
                            fps=config.exports_timing_fps))
        except:
            self.logger.exception('erreur lors de l\'écriture du fichier ')
            return
        self.logger.debug('fichier "katze_config.lua" en ordre')
        return True

    def install_latest(self):
        self.logger.debug('')
        if self.is_installed and not (self.is_corrupt or self.has_update):
            self.logger.info('les exports sont déjà correctement installés')
            return True

        if self.has_update or not self.side_exports_all_set:
            self.logger.info('installations des fichiers lua du KatzePit')
            self.logger.debug('téléchargement de la dernière version')
            tmp_file = tmp_path()
            if not Defer.download(self.download_url, tmp_file, download_nice_name="exports DCS", redirect=True):
                self.logger.error('erreur lors du téléchargement des fichiers')
                return

            self.logger.debug('décompression des fichiers')
            tmp_dir = tmp_path()
            if not Defer.unzip(tmp_file, tmp_dir):
                self.logger.error('erreur lors de la décompression')
                return

            self.logger.debug('copie des fichiers dans le répertoire d\'export')
            src_dir = abspath(join(tmp_dir, '{}-{}'
                                   .format(gh['atom'][self.dic_name]['name'], gh['branch'][config.branch])))
            if not all([copy(join(src_dir, x), self.install_dir) for x in self.side_exports_names]):
                self.logger.error('erreur lors de la copie des fichiers')
                return

        if not self.main_export_all_set:
            self.logger.info('installation de l\'export principal')
            if not self.setup_main_export():
                self.logger.error('erreur lors de l\'installation de l\'export principal')
                return

        if not self.install_sioc_config_lua():
            return

        if not exists(join(config.dcs_saved_games_dir, "logs", "KatzePit")):
            self.logger.debug("création du répertoire export pour le log des exports")
            mkdir(join(config.dcs_saved_games_dir, "logs", "KatzePit"))

        self.update_local_sha()
        self.update_remote_sha()

        return True

    def run(self):
        pass

    @property
    def can_run(self):
        return False


class Gui():
    def __init__(self):
        pass

    class LoggingHandler(QtCore.QThread, Handler):

        send_text = QtCore.pyqtSignal(str)

        def __init__(self):
            QtCore.QThread.__init__(self)
            Handler.__init__(self)
            self.q = Queue()

        def emit(self, record):
            if record.levelno > config.log_level * 10:
                self.q.put(record)

        @QtCore.pyqtSlot()
        def run(self):
            while True:
                record = self.q.get()
                text = self.format(record)
                self.send_text.emit(text)

    class Progress(QtCore.QThread):
        """
        Draw me like one of your French girls !
        """
        set_status_bar = QtCore.pyqtSignal(str)

        def __init__(self):
            QtCore.QThread.__init__(self)
            self.q = Queue()

        def update(self, text):
            self.q.put(text)

        @QtCore.pyqtSlot()
        def run(self):
            while True:
                text = self.q.get()
                self.set_status_bar.emit(text)

    class ProgressBar(QtCore.QThread):
        """
        I wanna see moving green bars full of delicious goo !
        """
        set_progress_bar = QtCore.pyqtSignal(int)
        set_progress_bar_title = QtCore.pyqtSignal(str)

        def __init__(self):
            QtCore.QThread.__init__(self)
            self.q = Queue()

        def update_title(self, text):
            logger.debug("GuiProgressBar - update_title: {}".format(text))
            self.set_progress_bar_title.emit(text)

        def update(self, i):
            # main_ui.progress_bar.setValue(round(i))
            self.set_progress_bar.emit(round(i))
            # self.q.put(round(i))

        @QtCore.pyqtSlot()
        def run(self):
            while True:
                i = self.q.get()
                self.set_progress_bar.emit(i)

    class Msgbox(QtCore.QThread):
        """
        You got mail
        """
        msg = QtCore.pyqtSignal(str)

        def __init__(self):
            QtCore.QThread.__init__(self)
            self.q = Queue()

        def say(self, text):
            logger.debug("GuiMsgbox - say: {}".format(text))
            self.q.put(text)

        @QtCore.pyqtSlot()
        def run(self):
            while True:
                title, text = self.q.get()
                self.msg.emit(text)

    class UpdateEKPI(QDialog, qt_update_ui.Ui_Dialog):
        class UpdateProgressBar():
            def __init__(self, parent):
                self.progress_bar = parent.progress_bar

            def update_title(self, title):
                pass

            def update(self, i):
                self.progress_bar.setValue(round(int(i)))

        def __init__(self):
            logger.debug("UpdateGui - init")
            QDialog.__init__(self)
            self.setupUi(self)
            self.setWindowTitle('Mise à jour d\'EKPI')
            self.setModal(True)
            self.setWindowIcon(ekpi_icon)
            self.download_progress_bar = Gui.UpdateEKPI.UpdateProgressBar(self)
            # self.show()
            # noinspection PyUnresolvedReferences

    # noinspection PyBroadException
    class Main(QMainWindow, QtCore.QObject, qt_main_ui.Ui_MainWindow):
        q_random_katze = Queue()
        q_new_version = Queue()
        sig_update_buttons = QtCore.pyqtSignal()

        sig_req_update = QtCore.pyqtSignal(str)

        @staticmethod
        def req_update(new_value):
            Gui.Main.sig_req_update.emit(new_value)

        @logged
        def __init__(self):

            QtCore.QObject.__init__(self)
            QMainWindow.__init__(self)
            progress.start()

            self.update_gui = Gui.UpdateEKPI()
            self.disable_sioc = False

            self.logger_thread = QtCore.QThread()
            self.logger_handler = Gui.LoggingHandler()
            formatter = Formatter('%(levelname)s - %(name)s - %(message)s')
            self.logger_handler.setFormatter(formatter)
            self.logger_handler.moveToThread(self.logger_thread)
            # noinspection PyUnresolvedReferences
            self.logger_thread.started.connect(self.logger_handler.run)
            self.logger_handler.send_text.connect(self.log)
            logger.addHandler(self.logger_handler)
            self.logger_thread.start()

            progress.set_status_bar.connect(self.write_to_status_bar)
            progress_bar.set_progress_bar.connect(self.update_progress_bar)
            progress_bar.set_progress_bar_title.connect(self.update_progress_bar_title)
            progress_bar.start()
            msgbox.msg.connect(self.msgbox)
            msgbox.start()
            self.setupUi(self)
            self.combo_log_level.addItems([index['log_levels'][k] for k in index['log_levels'].keys()])
            self.combo_log_level.setCurrentIndex(config.log_level)
            self.combo_gh_repo.addItems([index['gh_repos'][k] for k in index['gh_repos'].keys()])
            self.combo_gh_repo.setCurrentIndex(config.repo)
            self.combo_gh_branch.addItems([gh['branch'][k] for k in gh['branch'].keys()])
            self.combo_gh_branch.setCurrentIndex(config.branch)
            self.combo_fc3_pit.addItems([index['fc3_pit'][k].replace('KaTZ-Pit_', '').replace('.html', '')
                                         for k in index['fc3_pit'].keys()])
            self.combo_fc3_pit.setCurrentIndex(config.fc3_pit)
            self.setWindowTitle("EKPI {} {}"
                                .format(__version__, 'MODE TEST - MODE TEST - MODE TEST' if local_test else ''))
            self.generic_thread_pool = []
            self.generic_worker_pool = []

            self.generic_thread_pool2 = Defer.ThreadPool()

            self.main_thread = QtCore.QThread(self)
            self.generic_thread_pool2.moveToThread(self.main_thread)
            # noinspection PyUnresolvedReferences
            self.main_thread.started.connect(self.generic_thread_pool2.run)
            self.main_thread.start()

            self.random_katze = []
            self.random_katze_thread = QtCore.QThread()
            self.random_katze_worker = Defer.RandomKatzeWorker(self.q_random_katze)
            self.random_katze_worker.moveToThread(self.random_katze_thread)
            self.random_katze_worker.random_katze_found.connect(self.__show_random_katze)
            # noinspection PyUnresolvedReferences
            self.random_katze_thread.started.connect(self.random_katze_worker.run)

        def start(self):

            self.__show_default_katze()

            if gh['token'] is not None:
                self.gh_user_edit.setEnabled(False)
                self.gh_user_pass.setEnabled(False)

            if local_test:
                self.setStyleSheet('background-color: {}'.format('yellow'))
                self.logger.warning(
                    "ce jaune atroce pour bien signifier que vous êtes en mode test. Profitez bien ! xD")

            self.__make_connections()

            # sync values from and to disk
            if sioc.is_installed:
                if not config.sioc_port == sioc.ini_port:
                    config.sioc_port = sioc.ini_port
                    if link.is_installed:
                        link.config.sioc_port = config.sioc_port
            if link.is_installed:
                link.config.read()
                if 'link_port' in link.config.d.keys() and not config.link_port == link.config.link_port:
                    config.link_port = link.config.link_port
                    for x in huey_pit, mi8_pit, fc3_pit, ka50_pit:
                        if x.is_installed:
                            x.setup_link()

            if not config.sioc_dir:
                sioc_dir = join(environ['PROGRAMFILES(X86)'], 'iocards', 'sioc')
                if exists(join(sioc_dir, 'sioc.exe')):
                    config.sioc_dir = sioc_dir
                else:
                    config.sioc_dir = ''
                    self.logger.warning('je ne suis pas parvenu à trouver le répertoire de SIOC. '
                                        'Merci de bien vouloir me le renseigner, '
                                        'sans quoi mon existence n\'a plus aucun sens. Snif snif.')

            self.sioc_dir_lineEdit.setText(config.sioc_dir)

            if not check_is_admin() and 'Program Files' in config.sioc_dir and not local_test:
                self.disable_sioc = True
                self.logger.warning("EKPI ne s'exécute pas en mode administrateur; les fonctions d'installations et de "
                                    "configuration de SIOC ont été désactivées car ce dernier est installé dans "
                                    "'Program Files'. Faites attention si vous modifiez le port de SIOC,"
                                    " la modification "
                                    "ne sera enregistrée que lorsque vous redémarrerez EKPI en mode administrateur !")

            if not config.dcs_saved_games_dir:
                a_reg = ConnectRegistry(None, HKEY_CURRENT_USER)
                try:
                    with OpenKey(a_reg, r"SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\User Shell Folders") \
                            as aKey:
                        s = QueryValueEx(aKey, "{4C5C32FF-BB9D-43B0-B5B4-2D72E54EAAA4}")[0]
                        config.dcs_saved_games_dir = abspath(join(s, 'DCS'))
                except:
                    try:
                        with OpenKey(a_reg, r"SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Shell Folders") \
                                as aKey:
                            s = QueryValueEx(aKey, "{4C5C32FF-BB9D-43B0-B5B4-2D72E54EAAA4}")[0]
                            config.dcs_saved_games_dir = abspath(join(s, 'DCS'))
                    except:
                        config.dcs_saved_games_dir = ''
                        self.logger.warning('je ne suis pas parvenu à trouver votre répertoire "Saved Games". Merci de '
                                            'bien vouloir me le renseigner,'
                                            ' sans quoi mon existence n\'a plus aucun sens.')
            self.saved_games_dir_lineEdit.setText(config.dcs_saved_games_dir)

            if first_run:
                self.msgbox("Pensez à bien vérifier que les chemin vers le répertoire 'Saved Games/DCS' et "
                            "vers l'installation de SIOC sont corrects !<br /><br />Ces deux paramètres se trouvent "
                            "tout au dessus dans l'interface. Vous êtes prévenus =)<br /><br />"
                            "Aussi, SIOC doit être installé pour que le programme fonctionne.<br /><br />"
                            "<a href='http://www.opencockpits.com/index.php/en/download/item/"
                            "sioc-ver-50b5?category_id=39'>Télécharger SIOC</a>")

            # self.__defer(Defer.random_katze, self.__show_random_katze, self.q_random_katze)
            self.random_katze_thread.start()

            self.__defer(lambda: self.sig_update_buttons.emit(), None, None, defer_nice_name='sig_update_buttons')

            if check_new_v:
                # debug mechanism to spare Github API requests allowance
                self.__defer(Defer.check_for_new_version_of_ekpi, self.__check_for_new_version_finish,
                             self.q_new_version)
                self.__defer(Defer.retrieve_latest_versions_from_github, None, None)
                self.__defer(lambda: self.sig_update_buttons.emit(), None, None, defer_nice_name='sig_update_buttons')

        def signaled_buttons_update(self):
            # gotta workaroung that Pixmap limitation ("outside main thread not safe blah blah")
            self.__update_buttons_status()

        def __update_buttons_status(self):
            self.logger.debug('GUI - Main - mise à jour du status des atomes')
            for x in [
                (self.export_dcs_button, self.export_dcs_label, dcs_export, None),
                (self.link_button, self.link_label, link, self.link_run_button),
                (self.link_button, self.link_label, data_dico, self.link_run_button),
                (self.sioc_ssi_button, self.sioc_ssi_label, sioc, self.sioc_run_button),
                (self.pit_fc3_button, self.pit_fc3_label, fc3_pit, self.fc3_pit_run_button),
                (self.pit_huey_button, self.pit_huey_label, huey_pit, self.huey_pit_run_button),
                (self.pit_ka_button, self.pit_ka_label, ka50_pit, self.ka50_pit_run_button),
                (self.pit_mi_button, self.pit_mi_label, mi8_pit, self.mi8_pit_run_button),
            ]:
                # palette = QtGui.QPalette()
                if x[2].is_installed:
                    if x[2].is_corrupt:
                        color = 'red'
                        x[1].setText("corrompu")
                        x[0].setText("Réparer")
                        x[0].setEnabled(True)
                    elif x[2].has_update:
                        color = 'orange'
                        x[1].setText("mise à jour disponible")
                        x[0].setText("Mettre à jour")
                        x[0].setEnabled(True)
                    else:
                        color = 'green'
                        x[1].setText("A jour")
                        x[0].setText("")
                        x[0].setEnabled(False)
                    if x[3] is not None and x[2].can_run:
                        x[3].setEnabled(True)
                else:
                    color = None
                    x[0].setText("Installer")
                    x[1].setText("Pas installé")
                    # palette.setColor(QtGui.QPalette.Window, QtCore.Qt.darkRed)
                    x[0].setEnabled(True)
                if color is not None:
                    x[1].setStyleSheet('background-color: {}'.format(color))
                    # x[1].setPalette(palette)

            if self.disable_sioc:
                self.sioc_ssi_button.setEnabled(False)

        @staticmethod
        def __setup_installed_pits():
            for x in [mi8_pit, ka50_pit, fc3_pit, huey_pit]:
                if x.is_installed:
                    # print(x)
                    x.setup_link()

        @staticmethod
        def connect_edit_to_config(line_edit, config_value_name, *extra_funcs):
            """
            Connects GUI lineEdits to the Config object and updates value wherever needed

            :param line_edit: name of the GUI lineEdit object
            :param config_value_name: name of the value to update (eg: 'sioc_port')
            :param extra_funcs: optionnal list of functions to run when value is updated
            :return:
            """

            def set_value():
                config.__setattr__(config_value_name, line_edit.text())

            line_edit.editingFinished.connect(set_value)
            if extra_funcs:
                for func in extra_funcs:
                    line_edit.editingFinished.connect(func)
            line_edit.setText(config.__getattr__(config_value_name))

        # noinspection PyUnresolvedReferences
        def __make_connections(self):

            # Stupid monkey-patching
            self.sig_update_buttons.connect(self.__update_buttons_status)

            # Connect GUI lineEdits to values in Config
            self.combo_log_level.currentIndexChanged.connect(self.__connect_update_log_level)
            self.combo_gh_repo.currentIndexChanged.connect(self.__connect_update_repo)
            self.combo_gh_branch.currentIndexChanged.connect(self.__connect_update_branch)
            self.combo_fc3_pit.currentIndexChanged.connect(self.__connect_update_fc3_pit)
            self.sig_req_update.connect(self.req_update)

            for x in [
                (self.linkHoteEdit, 'link_hote', self.__setup_installed_pits),
                (self.linkPortEdit, 'link_port', self.__setup_installed_pits),
                (self.teamspeakHoteEdit, 'ts_hote'),
                (self.gh_user_edit, 'gh_user'),
                (self.gh_user_pass, 'gh_pass'),
                (self.exports_timing_fast, 'exports_timing_fast', dcs_export.install_sioc_config_lua),
                (self.exports_timing_slow, 'exports_timing_slow', dcs_export.install_sioc_config_lua),
                (self.exports_timing_fps, 'exports_timing_fps', dcs_export.install_sioc_config_lua),
                (self.siocHoteEdit, 'sioc_hote', dcs_export.install_sioc_config_lua),
                (self.siocPortEdit, 'sioc_port', dcs_export.install_sioc_config_lua,
                 sioc.set_ini_port, link.setup_config_file),
                # (self.siocPlageEdit, 'sioc_plage', dcs_export.install_sioc_config_lua),
            ]:

                self.connect_edit_to_config(*x)

                if 'hote' in x[1]:
                    self.qt_validate_ip(x[0])
                elif 'port' in x[1]:
                    self.qt_validate_port(x[0])
                elif 'timing' in x[1]:
                    self.qt_validate_timing(x[0])

            # Connect directories buttons
            self.buttonSelectSiocFolder.clicked.connect(self.user_select_sioc_dir)
            self.buttonSelectSavedGames.clicked.connect(self.user_select_saved_games_dir)
            self.buttonOpenSavedGamesDir.clicked.connect(lambda: self.open_dir(config.saved_games_dir))
            self.buttonOpenSiocDir.clicked.connect(lambda: self.open_dir(config.open_sioc_dir))
            self.button_ugly_cat.clicked.connect(self.random_katze_worker.run)
            # self.button_ugly_cat.clicked.connect(
            # lambda: self.__defer(Defer.random_katze, self.__show_random_katze, self.q_random_katze,
            # defer_nice_name='draw_a_French_girl')
            # )

            self.pit_mi_button.clicked.connect(
                lambda: self.__defer(mi8_pit.install_latest, self.__update_buttons_status, None,
                                     defer_nice_name='mi8_button_clicked'))
            self.pit_ka_button.clicked.connect(
                lambda: self.__defer(ka50_pit.install_latest, self.__update_buttons_status, None,
                                     defer_nice_name='ka50_button_clicked'))
            self.pit_fc3_button.clicked.connect(
                lambda: self.__defer(fc3_pit.install_latest, self.__update_buttons_status, None,
                                     defer_nice_name='fc3_button_clicked'))
            self.pit_huey_button.clicked.connect(
                lambda: self.__defer(huey_pit.install_latest, self.__update_buttons_status, None,
                                     defer_nice_name='huey_button_clicked'))
            self.export_dcs_button.clicked.connect(
                lambda: self.__defer(dcs_export.install_latest, self.__update_buttons_status, None,
                                     defer_nice_name='export_dcs_button_clicked'))
            self.sioc_ssi_button.clicked.connect(
                lambda: self.__defer(sioc.install_latest, self.__update_buttons_status, None,
                                     defer_nice_name='sioc_ssi_button_clicked'))
            self.link_button.clicked.connect(
                lambda: self.__defer(link.install_latest, self.__update_buttons_status, None,
                                     defer_nice_name='link_button_clicked'))

            self.mi8_pit_run_button.clicked.connect(mi8_pit.run)
            # lambda: self.__defer(mi8_pit.run, None, None))
            self.ka50_pit_run_button.clicked.connect(ka50_pit.run)
            # lambda: self.__defer(ka50_pit.run, None, None))
            self.fc3_pit_run_button.clicked.connect(fc3_pit.run)
            # lambda: self.__defer(fc3_pit.run, None, None))
            self.huey_pit_run_button.clicked.connect(huey_pit.run)
            # lambda: self.__defer(huey_pit.run, None, None))
            self.sioc_run_button.clicked.connect(sioc.run)
            # lambda: self.__defer(sioc.run, None, None))
            self.link_run_button.clicked.connect(link.run)

        def __defer(self, func, sig_func, q, *args, defer_nice_name=None, **kwargs):
            """
            Defers execution of a function to the sequential ThreadPool.

            This can be used to spam a large amount of operations that need to be done, ensuring they'll
            be safely executed one after another, in a threaded environment.
            :param func: actual function to run
            :param sig_func: function that'll be run on worker.finished.emit (or None)
            :param q: queue that'll store function result (or None)
            :param args: func(*args)
            :param kwargs: func(*args, **kwargs)
            :return: nothing
            """
            if defer_nice_name is None:
                defer_nice_name = func.__name__
            self.logger.debug("MainGui - __defer - {}".format(defer_nice_name))
            w, t = self.spawn_generic_worker(q, func, defer_nice_name, *args, **kwargs)
            if sig_func is not None:
                w.finished.connect(sig_func)
            t.start()

        def __connect_update_log_level(self):
            self.logger.debug("MainGUI - __connect_update_log_level")
            config.log_level = self.combo_log_level.currentIndex()
            self.logger.debug("verbosité sélectionnée: {}".format(index['log_levels'][config.log_level]))

        def __connect_update_branch(self):
            self.logger.debug('')
            config.branch = self.combo_gh_branch.currentIndex()
            if check_new_v:
                # debug mechanism to spare Github API requests allowance
                self.__defer(Defer.retrieve_latest_versions_from_github, self.__update_buttons_status, None)
                # self.__defer(lambda: self.sig_update_buttons.emit(), None, None, defer_nice_name='sig_update_buttons')
            self.logger.debug('done')

        def __connect_update_repo(self):
            self.logger.debug('')
            config.repo = self.combo_gh_repo.currentIndex()
            if check_new_v:
                # debug mechanism to spare Github API requests allowance
                self.__defer(Defer.retrieve_latest_versions_from_github, self.__update_buttons_status, None)
                # self.__defer(lambda: self.sig_update_buttons.emit(), None, None, defer_nice_name='sig_update_buttons')
            self.logger.debug('done')

        def __connect_update_fc3_pit(self):
            self.logger.debug('')
            config.fc3_pit = self.combo_fc3_pit.currentIndex()
            self.logger.debug('pit FC3 sélectionné: {}'.format(index['fc3_pit'][config.fc3_pit]))

        def __check_for_new_version_finish(self):
            """
            Process the results of defer_check_for_new_version
            :return: nothing
            """
            download_url, download_size, release_notes, tag = self.q_new_version.get(False, 2)
            if download_url:
                text = "<p>Une nouvelle version d'EKPI est disponible: {}</p>" \
                       "<p>Voulez-vous mettre à jour maintenant ?<br>" \
                       "(EKPI sera automatiquement redemarré)</p>" \
                       "<p>Notes de version: {}</p>".format(tag, release_notes)
                if self.confirm(text):
                    self.hide()
                    self.update_gui.show()
                    tmp_file = tmp_path()
                    w, t = self.spawn_generic_worker(None,
                                                     Defer.download,
                                                     'EKPI_check_new_version',
                                                     download_url,
                                                     tmp_file,
                                                     callback=self.update_gui.download_progress_bar, size=download_size,
                                                     download_nice_name='EKPI')
                    w.finished.connect(lambda: self.__unzip_new_version(tmp_file))
                    t.start()

        def __unzip_new_version(self, tmp_file):
            """
            Installs the new version
            :param tmp_file: location of the previsouly downloaded new version
            :return: nothing
            """
            if exists('./update'):
                rmtree('./update')
            mkdir('./update')
            w, t = self.spawn_generic_worker(None, Defer.unzip, 'EKPI_new_version_unzip', tmp_file, './update')
            self.update_gui.install_label.setEnabled(True)
            t.start()
            w.finished.connect(self.__spawn_new_version_process)

        def __spawn_new_version_process(self):
            self.logger.debug("MainGui - fermeture du programme")
            for x in listdir('./update'):
                if x == "ekpi.exe":
                    continue
                try:
                    copy('./update/{}'.format(x), x)
                except:
                    continue
            rmtree('./update')
            self.update_gui.hide()
            if local_test:
                self.destroy()
                self.__exit(call('ekpi.exe test'))
            else:
                startfile('ekpi.exe')
                _exit(0)

        @staticmethod
        def __exit(ret=0):
            logger.debug("MainGui - fermeture du programme")
            _exit(ret)

        def __show_default_katze(self):
            pic = QtGui.QPixmap(':/pics/default_katze.jpg')
            scaled_pic = pic.scaled(self.random_pic.size(), QtCore.Qt.KeepAspectRatio)
            self.random_pic.setPixmap(scaled_pic)

        def __show_random_katze(self):
            """
            Display a magnificent specimen of Katze, previously stored in memory
            :return: nothing
            """
            data = self.q_random_katze.get(False, 2)
            if data:
                pic = QtGui.QImage()
                pic.loadFromData(data)
                pic = QtGui.QPixmap(pic)
                scaled_pic = pic.scaled(self.random_pic.size(), QtCore.Qt.KeepAspectRatio)
                self.random_pic.setPixmap(scaled_pic)
            else:
                self.logger.debug('impossible d\'obtenir une jolie française à dessiner, même après 10 tentatives ...')

        def __queue_generic_worker(self, t, w, defer_nice_name=None):
            """
            Places worker & thread in the sequential queue when the thread is started

            Placing a started thread in the queue puts its worker on a waiting list. As soon as all previously queued
            workers are done (= emitted finished), its task will be started

            :param t: thread
            :param w: worker
            :param defer_nice_name: nice name for function to be run (debugging purpose only)
            :return: nothing
            """
            self.generic_thread_pool2.add(w, t, defer_nice_name)

        def spawn_generic_worker(self, ret_q, function, defer_nice_name, *args, **kwargs):
            """
            Creates a self-destructing worker in its own thread.

            Thread and Worker are returned (as a tuple) before execution for signals interfacing. Thread & Worker are
            both started via "thread.start()", and both get flagged for GC when Worker emits "finished".

            :param ret_q: return queue: function's return will be put here is ret_q is not None
            :param function: function to be run by Worker
            :param args: args of function
            :param kwargs: keyword args of function
            :return: (Worker, Thread)
            """
            # Threads & Workers are put in a never-to-be-used List so they belong to the Gui object instead of being
            # limited to the scoper of this function
            self.generic_thread_pool.append(QtCore.QThread(self))
            t = self.generic_thread_pool[-1]
            self.generic_worker_pool.append(Defer.GenericWorker(ret_q, function, *args, **kwargs))
            w = self.generic_worker_pool[-1]
            w.moveToThread(t)

            # Pushing Worker into sequential queue
            t.started.connect(lambda: self.__queue_generic_worker(t, w, defer_nice_name))

            # Standard Qt stuff
            w.finished.connect(t.quit)
            w.finished.connect(w.deleteLater)
            t.finished.connect(t.deleteLater)
            return w, t

        @QtCore.pyqtSlot(str)
        def write_to_status_bar(self, text):
            self.statusbar.showMessage(text, 3000)

        def user_select_sioc_dir(self):
            # noinspection PyTypeChecker
            directory = QFileDialog.getExistingDirectory(None, "Sélectionnez le répertoire d'installation de SIOC")
            if directory:
                if not exists(join(directory, "sioc.exe")):
                    self.logger.error("hmmm, je n'ai pas trouvé le fichier 'sioc.exe' "
                                      "dans le répertoire '{}', ça ne va pas le faire"
                                      .format(directory))
                    return
                config.sioc_dir = directory
                self.sioc_dir_lineEdit.setText(directory)
                self.logger.info("répertoire SIOC mis à jour")

        def user_select_saved_games_dir(self):
            # noinspection PyTypeChecker
            directory = QFileDialog.getExistingDirectory(
                None, "Sélectionnez le répertoire 'DCS' dans les 'Parties enregistrées'")
            if directory:
                config.dcs_saved_games_dir = directory
                self.saved_games_dir_lineEdit.setText(directory)
                self.logger.info("répertoire 'saved games' mis à jour")

        @staticmethod
        def __build_msg_box():
            _msgbox = QMessageBox()
            _msgbox.setWindowTitle("EKPI {}".format(__version__))
            _msgbox.setWindowIcon(ekpi_icon)
            _msgbox.setTextFormat(1)
            return _msgbox

        @QtCore.pyqtSlot(str)
        def msgbox(self, text):
            _msgbox = self.__build_msg_box()
            _msgbox.setText(text)
            _msgbox.addButton(QPushButton('Ok ...'), QMessageBox.YesRole)
            _msgbox.exec_()

        @QtCore.pyqtSlot(str)
        def confirm(self, text):
            _msgbox = self.__build_msg_box()
            _msgbox.setText(text)
            _msgbox.addButton(QMessageBox.Yes)
            _msgbox.addButton(QMessageBox.No)
            if _msgbox.exec_() == QMessageBox.Yes:
                # self.q_confirm.put("yes")
                return True
                # else:
                # self.q_confirm.put("nope")
            return

        @QtCore.pyqtSlot(int)
        def update_progress_bar(self, i):
            self.progress_bar.setValue(i)

        @QtCore.pyqtSlot(str)
        def update_progress_bar_title(self, text):
            self.progress_bar_label.setText(text)

        @QtCore.pyqtSlot(str)
        def req_update(self, text):
            self.label_remaining_requests.setText(text)

        @QtCore.pyqtSlot(str)
        def log(self, text):
            self.log_window.moveCursor(QtGui.QTextCursor.End)
            self.log_window.append(text)

        @staticmethod
        def open_dir(dir_path):
            Popen(r'explorer "{}"'.format(dir_path))

        @staticmethod
        def qt_validate_ip(qt_ctl):
            qt_ctl.setValidator(QtGui.QRegExpValidator(RE_QT_IP_ADDRESS_VALIDATOR))

        @staticmethod
        def qt_validate_port(qt_ctl):
            qt_ctl.setValidator(QtGui.QIntValidator(1, 65536))

        @staticmethod
        def qt_validate_timing(qt_ctl):
            qt_ctl.setValidator(QtGui.QIntValidator(100, 2000))

# region Setup environment for static functions
# initialize low-level classes for subsqequent calls in static functions definitions
logger = mkLogger('__main__')
config = Config()
progress = Gui.Progress()
progress_bar = Gui.ProgressBar()
msgbox = Gui.Msgbox()

# ultra low-level is dealt with, replace those naughty builtins
BuiltinWrapper.redeclare_builtins()

dcs_export = DCSExport()
sioc = SIOC()
link = KatzeLink()
data_dico = KatzeLink.DataDico()
ka50_pit = KatzePit('ka50_pit')
mi8_pit = KatzePit('mi8_pit')
fc3_pit = KatzePit('fc3_pit')
huey_pit = KatzePit('huey_pit')


# noinspection PyBroadException
class Defer():
    def __init__(self):
        pass

    class ThreadPool(QtCore.QObject):
        """
        Provides a sequential pool (Queue) for self-terminating threads
        """

        @logged
        def __init__(self):
            QtCore.QObject.__init__(self)
            self.q = Queue()
            self.free = True

        def add(self, w, t, defer_nice_name=None):
            if defer_nice_name is None:
                defer_nice_name = w.function.__name__
            self.logger.debug("{}".format(defer_nice_name))
            self.q.put((w, t, defer_nice_name))

        def free_me(self):
            self.free = True

        @QtCore.pyqtSlot()
        def run(self):
            while True:
                if self.free is True:
                    self.free = False
                    w, t, defer_nice_name = self.q.get()
                    self.logger.debug("{} - démarré".format(defer_nice_name))
                    w.finished.connect(self.free_me)
                    w.task()
                    self.logger.debug("{} - terminé".format(defer_nice_name))

    class GenericWorker(QtCore.QObject):
        finished = QtCore.pyqtSignal()
        error = QtCore.pyqtSignal()
        success = QtCore.pyqtSignal()

        @logged
        def __init__(self, ret_q, function, *args, **kwargs):
            self.logger.debug("{} ({} - {})".format(function.__name__, args, kwargs))
            QtCore.QObject.__init__(self)
            self.function = function
            self.args = args
            self.kwargs = kwargs
            self.ret_q = ret_q

        def task(self):
            try:
                ret = self.function(*self.args, **self.kwargs)
            except:
                if self.ret_q is not None:
                    self.ret_q.put(None)
                self.logger.exception("erreur lors de l'exécution de la tâche")
                self.error.emit()
                self.finished.emit()
                raise
            if self.ret_q is not None:
                self.ret_q.put(ret)
            self.success.emit()
            self.finished.emit()

    # noinspection PyBroadException
    class RandomKatzeWorker(QtCore.QObject):

        random_katze_found = QtCore.pyqtSignal()

        logger = None

        @logged
        def __init__(self, ret_q):
            QtCore.QObject.__init__(self)
            self.q = ret_q

        @QtCore.pyqtSlot()
        def run(self):
            self.logger.debug('')
            global shuffled_katze
            data = False
            while not data:
                if len(shuffled_katze) == 0:
                    self.logger.debug('réinitialisation de la portée de chatons')
                    shuffled_katze = [x for x in random_katze]
                    shuffle(shuffled_katze)
                count = 1
                while count < 10:
                    self.logger.debug('essai {} de 10'.format(count))
                    try:
                        data = request.urlopen(shuffled_katze.pop(), timeout=5).read()
                        break
                    except:
                        count += 1
            self.logger.debug('données obtenues')
            self.q.put(data)
            self.random_katze_found.emit()

    @staticmethod
    def check_for_new_version_of_ekpi():
        if not hasattr(sys, 'frozen'):
            return False, False, False, False
        logger.debug("Defer - check_for_new_version_of_ekpi - "
                     "vérification de l'existence d'une nouvelle version d'EKPI")
        r = requests_get('https://api.github.com/repos/etcher3rd/EKPI/releases')
        if not r:
            logger.error("Defer - check_for_new_version_of_ekpi - erreur lors de la requete HTTP")
            return False, False, False, False
        try:
            for x in r.json():
                tag = x['tag_name']
                url = x['assets'][0]['browser_download_url']
                size = x['assets'][0]['size']
                notes = x['body']
                draft = x['draft']
                prerelease = x['prerelease']
                logger.debug(notes)
                logger.debug(draft)
                logger.debug(prerelease)
                # skip all alpha & beta versions if we're not already on it
                if 'alpha' in tag and not 'alpha' in __version__ \
                        or 'beta' in tag and not 'beta' in __version__:
                    continue
                # we're already up to date
                elif tag == __version__:
                    return False, False, False, False
                # return latest version
                return url, size, notes, tag
        except IndexError:
            logger.error('erreur lors de la recherche de mise à jour; la dernière release n\'a pas encore de '
                         'fichier disponible au téléchargement, probablement qu\'etcher est en train d\'uploader,'
                         'ou qu\'il a retiré la release à cause d\'un bug découvert à la dernière minute. Vous '
                         'pourrez réessayer d\'ici quelques minutes.')
        except:
            logger.exception("Defer - check_for_new_version_of_ekpi - "
                             "erreur lors de la recherche d'une nouvelle version")
            return False, False, False, False
        logger.debug("Defer - check_for_new_version_of_ekpi - pas de nouvelle version")
        return False, False, False, False

    # noinspection PyBroadException
    @staticmethod
    def download(url, target, count=1, size=None, download_nice_name='', callback=progress_bar, redirect=False):
        logger.debug("Defer - download - début du téléchargement")
        logger.debug("Defer - download - url: {}".format(url))
        logger.debug("Defer - download - fichier local: {}".format(target))
        try:
            logger.debug("Defer - download - récupération des headers")
            resp = requests_head(url)
        except:
            logger.exception("Defer - download - erreur lors de la récupération des headers")
            return
        if not resp:
            logger.exception("Defer - download - erreur lors de la récupération des headers")
            return
        logger.debug('Defer - download - lecture des headers')
        try:
            h = resp.headers
            if size is None:
                logger.debug('Defer - download - recherche de la taille du fichier à télécharger dans les headers')
                if 'Content-Length' in h.keys():
                    size = h['Content-Length']
                    logger.debug('Defer - download - taille trouvée: {}'.format(size))
        except:
            logger.error('defer-download - erreur lors de la lecture des headers')
            return
        if redirect and 'location' in h.keys() and not h['location'] == url:
            logger.debug('Defer - download - redirection vers la nouvelle url: {}'.format(h['location']))
            if count > 10:
                logger.error("Defer - download - trop de redirections, je laisse tomber")
                return
            return Defer.download(h['location'], target, count + 1, size=size,
                                  download_nice_name=download_nice_name, redirect=redirect)
        if size is None:
            if count > 10:
                logger.error('Defer - download - je ne suis pas parvenu à obtenir la taille du fichier distant')
                logger.error('Defer - download - headers: {}'.format(h))
                return
            return Defer.download(url, target, count=count + 1, size=size,
                                  download_nice_name=download_nice_name, callback=callback, redirect=redirect)
        else:
            size = int(size)
        logger.debug("Defer - download - début du transfert des données")
        try:
            callback.update(0)
            callback.update_title("Téléchargement de : {} ({})".format(download_nice_name, humansize(size)))
            with request.urlopen(url) as resp, open(target, mode='wb') as f:
                size_dl = 0
                while True:
                    buffer = resp.read(512)
                    if not buffer:
                        break
                    size_dl += len(buffer)
                    f.write(buffer)
                    callback.update(size_dl * 100 / size)
        except:
            logger.exception("Defer - download - erreur lors du transfert des données")
            return
        callback.update_title("Téléchargement de : {} ({}) - succès".format(download_nice_name, humansize(size)))
        progress_bar.update(100)
        return True

    # noinspection PyBroadException
    @staticmethod
    def random_katze():
        global shuffled_katze
        if len(shuffled_katze) == 0:
            shuffled_katze = [x for x in random_katze]
            shuffle(shuffled_katze)
        count = 1
        while count < 10:
            try:
                return request.urlopen(shuffled_katze.pop(), timeout=2).read()
            except:
                count += 1

    # noinspection PyBroadException
    @staticmethod
    def unzip(zip_file_path, target_dir):
        logger.debug("Defer - unzip - ouverture du fichier ZIP")
        try:
            with ZipFile(zip_file_path, mode='r', compression=ZIP_LZMA) as zip_file:
                logger.debug(
                    "Defer - unzip - extraction des données vers {}".format(target_dir))
                zip_file.extractall(target_dir)
        except BadZipfile:
            logger.exception(
                "il semble que le fichier ZIP suivant soit corrompu: {}".format(zip_file_path))
            return
        except:
            logger.exception("Defer - unzip- erreur lors de l'extraction du fichier ZIP")
            msgbox.say("Est-ce que le dossier suivant est protégé par l'UAC Windows ? \n\"{}\""
                       "\n\nSi oui, il faudrait peut-être redémarrer EKPI en mode administrateur"
                       .format(zip_file_path))
            return
        return True

    # noinspection PyBroadException
    @staticmethod
    def retrieve_latest_versions_from_github():
        logger.debug("Defer - retrieve_latest_versions_from_github - recherches des dernières versions")
        count_total = len(gh['atom'])
        count_current = 1
        progress_bar.update_title("Recherche des dernières versions")
        progress_bar.update(0)

        for k in gh['atom'].keys():

            if k in ['link', 'dcs_export']:
                # we'll do Miaou Link later
                continue

            progress_bar.update(count_current * 100 / count_total)
            try:
                logger.debug("Defer - retrieve_latest_versions_from_github - recherche de version pour: {}".format(k))
                r = requests_get("https://api.github.com/repos/{}/{}/commits/{}"
                                 .format(gh['owner'][config.repo], gh['atom'][k]['name'], gh['branch'][config.branch]))
                # headers={'sha': gh['branch'][config.branch]})
                if not r:
                    logger.error("erreur lors de la recherche des dernières version. Github impose une limite de 60 "
                                 "recherches par heures, est-ce possible que vous l'ayez dépassée ? Ou alors les "
                                 "identifiants GitHub ne sont pas corrects ? C'est aussi possible que la branche {} "
                                 "n'existe pas pour {} sur le repo de {}"
                                 .format(gh['branch'][config.branch], gh['atom'][k]['name'], gh['owner'][config.repo]))
                    progress_bar.update(0)
                    progress_bar.update_title('')
                    continue
                if type(r.json()) == list:
                    myobj = r.json()[0]
                else:
                    myobj = r.json()
                if 'sha' in myobj.keys():
                    gh['atom'][k]['latest'] = myobj['sha']
                else:
                    logger.error(
                        "Defer - retrieve_latest_versions_from_github - réponse innatendue de la part de l'API Github")
                    return
            except:
                logger.exception(
                    "Defer - retrieve_latest_versions_from_github - erreur lors de la recherche des dernières versions")
                return
            count_current += 1

        r = requests_get("https://api.github.com/repos/{}/Link/releases".format(gh['owner'][config.repo]))
        if not r:
            logger.error("Erreur lors de la recherche des dernières versions. Github impose une limite de 60 "
                         "recherches par heures, est-ce possible que vous l'ayez dépassée ?")
            progress_bar.update(0)
            progress_bar.update_title('')
            return
        try:
            myobj = r.json()[0]
            if 'tag_name' in myobj.keys():
                gh['atom']['link']['latest'] = myobj['tag_name']
                gh['atom']['link']['download_url'] = myobj['assets'][0]['browser_download_url']
                gh['atom']['link']['download_size'] = myobj['assets'][0]['size']
            else:
                logger.error(
                    "Defer - retrieve_latest_versions_from_github - réponse innatendue de la part de l'API Github")
                return
        except IndexError:
            logger.error('erreur lors de la recherche de mise à jour; la dernière release n\'a pas encore de '
                         'fichier disponible en téléchargement, probablement qu\'etcher est en train d\'uploader,'
                         'ou qu\'il a retiré la release à cause d\'un bug découvert à la dernière minute. Vous '
                         'pourrez réessayer d\'ici quelques minutes.')
            progress_bar.update(100)
            return
        progress_bar.update(100)
        progress_bar.update_title("Recherche des dernières versions terminée")
        return True


def tmp_path():
    return abspath(join(gettempdir(), ''.join(choice("{0}{1}".format(ascii_uppercase, digits)) for _ in range(15))))


# noinspection PyBroadException
def path_checksum(directory, includes=None, excludes=None, blocksize=4096):
    """
    Returns the aggregated SHA1 of the directory and all its children

    :param directory: path to walk
    :param includes: exclusive list of file to include in the scan
    :param excludes: list of file to exclude from the scan
    :return: SHA1, or negative number in case of error
    """
    logger.debug("path_checksum - compilation du SHA1 pour le répertoire: {}".format(directory))
    logger.debug("path_checksum - includes: {}".format(includes))
    logger.debug("path_checksum - excludes: {}".format(excludes))
    sha_hash = sha1()
    if not exists(directory):
        logger.debug("path_checksum - le répertoire n'existe pas")
        return -1
    try:
        for root, dirs, files in walk(directory):
            for names in files:
                if (not includes is None and not names in includes) \
                        or (excludes is not None and names in excludes):
                    continue
                filepath = join(root, names)
                try:
                    with open(filepath, mode='rb') as f1:
                        while 1:
                            buf = f1.read(blocksize)
                            if not buf:
                                break
                            sha_hash.update(buf)
                except:
                    logger.warning("path_checksum - erreur lors de la lecture du fichier: {}".format(filepath))
                    return -2
    except:
        logger.exception("path_checksum - erreur lors de la compilation du SHA1 pour le répertoire: {}"
                         .format(directory))
        return -3
    ret = sha_hash.hexdigest()
    # logger.debug("path_checksum - SHA obtenu: {}".format(ret))
    return ret


def humansize(nbytes):
    suffixes = ['B', 'KB', 'MB', 'GB', 'TB', 'PB']
    if nbytes == 0:
        return '0 B'
    i = 0
    while nbytes >= 1024 and i < len(suffixes) - 1:
        nbytes /= 1024.
        i += 1
    f = ('%.2f' % nbytes).rstrip('0').rstrip('.')
    return '%s %s' % (f, suffixes[i])

# endregion

# random globals
first_run = not exists(config.path)
check_new_v = True
local_test = False
while len(sys.argv) > 1:
    arg = sys.argv.pop()
    if arg == 'nocheck':
        check_new_v = False
    if arg == 'test':
        local_test = True

# start the actual program
qt_app = QApplication(sys.argv)
ekpi_icon = QtGui.QIcon(':/icons/ekpi.ico')
main_ui = Gui.Main()
main_ui.setWindowIcon(ekpi_icon)
main_ui.show()
main_ui.start()

# wait for EventLoop to return
_exit(qt_app.exec())